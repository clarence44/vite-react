import { defineConfig } from 'vite';
import reactRefresh from '@vitejs/plugin-react-refresh';
import { resolve } from 'path';


// https://vitejs.dev/config/
export default defineConfig({
  // 目录别名
  resolve: {
    alias: {
      '@': resolve(__dirname, './src'),
    },
  },
  plugins: [reactRefresh()],
});
