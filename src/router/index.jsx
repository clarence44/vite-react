import React, {Component} from 'react';
import {Switch, BrowserRouter as Router, Route, Link} from 'react-router-dom';
import Home from '@/pages/Home';
import About from '@/pages/About';
import Detail from '@/pages/Detail';

export default class Routers extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/about" component={About} />
          <Route path="/detail" component={Detail} />
          {/* <Route path="/login" component={Login} /> */}
        </Switch>
      </Router>
     
    )
  }
}
