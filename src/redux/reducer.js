import { combineReducers } from "redux";

// 默认值
const API_STATE = {};

export default combineReducers({
  apiData: (state = API_STATE, action) => {
    state[action.type] = action.data;
    return { ...state };
  },
});
