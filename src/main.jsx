import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import configStore from './redux/store';
import Router from './router';
// import App from './App';
import './index.css';
import 'antd/dist/antd.css'; // antd样式

const store = configStore();

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <Router />
    </BrowserRouter>
  </Provider>,
  document.getElementById('root')
);
