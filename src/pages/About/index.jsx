import React from 'react';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';

export default function App() {
  const state = useSelector((state) => state.apiData);

  React.useEffect(() => {
    console.log(state)
  }, []);

  return (
    <div className="App">
      <Link to="/">点击我跳转到 home</Link>
    </div>
  );
}
