import React from 'react';
// import Pokemons from "./features/Pokemons";
import logo from '@/logo.svg';
import './index.less';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { Button } from 'antd';

export default function App() {
  const state = useSelector((state) => state);
  const dispatch = useDispatch();

  React.useEffect(() => {
    console.log('123', state);
    dispatch({ type: 'abc', data: 11 });
  }, []);

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>Hello Vite + React + Redux Toolkit Query!</p>
        {/* <Pokemons /> */}
        <p>
          Edit <code>App.jsx</code> and save to asdf test HMR updates.
        </p>
        <p>
          <Link to="about">点击我跳转到 about</Link>
        </p>
        <p>
          <Link to="detail">点击我跳转到 detail</Link>
        </p>
        <p>
          <Button type="primary" onClick={()=>{console.log('antd')}}>antd button</Button>
        </p>

        <p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
          {' | '}
          <a
            className="App-link"
            href="https://vitejs.dev/guide/features.html"
            target="_blank"
            rel="noopener noreferrer"
          >
            Vite Docs
          </a>
          {' | '}
          <a
            className="App-link"
            href="https://redux-toolkit.js.org/tutorials/rtk-query"
            target="_blank"
            rel="noopener noreferrer"
          >
            RTK Query
          </a>
        </p>
      </header>
    </div>
  );
}
